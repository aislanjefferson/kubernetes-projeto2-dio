#!/bin/bash
# shell script para criar as imagens docker do backend e do database e envia para o repositorio do dockerhub

# cria os servicos no kubernetes
echo "Criando servicos no kubernetes"
kubectl apply -f services.yml --record

# cria os deployments no kubernetes
echo "Criando deployments no kubernetes"
kubectl apply -f deployment.yml --record
